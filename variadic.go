package main

import "fmt"

func _sum(nums ...int) {
	fmt.Println(nums, "  ")

	total := 0
	for _, num:= range nums {
		total += num
	}

	fmt.Println(total)
}

func main() {
	_sum(1, 2)
	_sum(3, 4, 5)

	nums := []int{9, 8 ,7, 6}
	_sum(nums...)
}

