package main

import (
	"fmt"
)

const (
	a = iota
	b
	c
)

func sum(a, b int) int {
	return a + b
}

func init() {
	fmt.Println("init", sum(a, b))
}

func main() {
	res := sum(4, 5)
	fmt.Println("main", res, c)
}
