package main

import (
	"fmt"
	"time"
)

func main() {
	i := 2

	fmt.Println("write ", i, " as ")

	switch i {
	case 1:
		fmt.Println(1)
	case 2:
		fmt.Println(2)
	}

	switch time.Now().Weekday() {
	case time.Saturday, time.Sunday:
		fmt.Println("weekend")
	default:
		fmt.Println("workday")
	}

	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("before noon")
	default:
		fmt.Println("after noon")
	}

	whatAmI := func(i interface{}) {
		switch t := i.(type) {
		case bool:
			fmt.Println("bool")
		case int:
			fmt.Println("int")
		default:
			fmt.Println("idk: ", t)
		}
	}

	whatAmI(true)
	whatAmI(1)
	whatAmI("hi")
}
